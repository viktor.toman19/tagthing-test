<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;
use App\Entity\Reservation;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $products = $entityManager->getRepository(Product::class)->findAll();
        $reservations = $entityManager->getRepository(Reservation::class)->findAll();

        return $this->render('index/index.html.twig', [
            'products' => $products,
            'reservations' => $reservations
        ]);
    }

    /**
     * @Route("/new-product", name="new_product")
     */
    public function newProduct(): Response
    {
        return $this->render('index/newProduct.html.twig');
    }

    /**
     * @Route("/save-product", name="save_product")
     */
    public function saveProduct(): Response
    {
        $request = Request::createFromGlobals();
        $entityManager = $this->getDoctrine()->getManager();

        try {
            $product = new Product();
            $product->setName($request->get('name'));
            $product->setBasePrice($request->get('base_price'));
            $product->setFinalPricesSum(0);
    
            $entityManager->persist($product);
            $entityManager->flush();
        } catch(Exception $e) {
            die($e);
        }

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/edit-product/{id}", name="edit_product")
     */
    public function editProduct($id)
    {
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        return $this->render('index/editProduct.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/update-product/{id}", name="update_product")
     */
    public function updateProduct($id): Response
    {
        $request = Request::createFromGlobals();
        $entityManager = $this->getDoctrine()->getManager();

        try {
            $product = $entityManager
            ->getRepository(Product::class)
            ->find($id);

            $productBasePrice = $request->get('base_price');
            $product->setName($request->get('name'));

            if ($productBasePrice != $product->getBasePrice()) {
                $product->setBasePrice($productBasePrice);
                $product->setFinalPricesSum($product->getUpdatedFinalPricesSum($entityManager, $productBasePrice));
            }

            $entityManager->persist($product);
            $entityManager->flush();
        } catch(DBALException $e) {
            die($e);
        }

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/new-reservation", name="new_reservation")
     */
    public function newReservation(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $products = $entityManager->getRepository(Product::class)->findAll();

        return $this->render('index/newReservation.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/save-reservation", name="save_reservation")
     */
    public function saveReservation(): Response
    {
        $request = Request::createFromGlobals();
        $entityManager = $this->getDoctrine()->getManager();

        try {
            $product = $entityManager
            ->getRepository(Product::class)
            ->find($request->get('product'));

            if (!$product) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $productBasePrice = $product->getBasePrice();

            $reservation = new Reservation();
            $reservation->setProduct($product);
            $reservation->setBasePrice($productBasePrice);
            $reservation->setFinalPrice($productBasePrice);
            
            $product->setFinalPricesSum($product->calculateFinalPricesSum());

            $entityManager->persist($reservation);
            $entityManager->persist($product);

            $entityManager->flush();
        } catch(DBALException $e) {
            die($e);
        }

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/edit-reservation/{id}", name="edit_reservation")
     */
    public function editReservation($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $reservation = $entityManager
            ->getRepository(Reservation::class)
            ->find($id);

        $products = $entityManager->getRepository(Product::class)->findAll();

        if (!$reservation) {
            throw $this->createNotFoundException(
                'No reservation found for id '.$id
            );
        }

        return $this->render('index/editReservation.html.twig', [
            'reservation' => $reservation,
            'products' => $products
        ]);
    }

    /**
     * @Route("/update-reservation/{id}", name="update_reservation")
     */
    public function updateReservation($id): Response
    {
        $request = Request::createFromGlobals();
        $entityManager = $this->getDoctrine()->getManager();

        try {
            $reservation = $entityManager
            ->getRepository(Reservation::class)
            ->find($id);

            $product = $entityManager
            ->getRepository(Product::class)
            ->find($request->get('product'));

            if (!$product) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }
            
            $reservationNewFinalPrice = $request->get('final_price');
            $reservation->setProduct($product);
            
            if ($reservationNewFinalPrice != $reservation->getFinalPrice()) {
                $product->setFinalPricesSum($reservation->getUpdatedFinalPricesSum($entityManager, $reservationNewFinalPrice));
                $reservation->setFinalPrice($reservationNewFinalPrice);
                $entityManager->persist($product);
            } else {
                $reservation->setBasePrice($reservation->getBasePrice());
            }

            $entityManager->persist($reservation);
            $entityManager->flush();
        } catch(DBALException $e) {
            die($e);
        }

        return $this->redirectToRoute('index');
    }

    
    /**
     * @Route("/delete-reservation/{id}", name="delete_reservation")
     */
    public function deleteReservation($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $reservation = $entityManager
            ->getRepository(Reservation::class)
            ->find($id);

        $product = $reservation->getProduct();
        $newFinalPriceSum = $product->getFinalPricesSum() - $reservation->getFinalPrice();

        $entityManager->remove($reservation);
        $entityManager->flush();

        $product->setFinalPricesSum($newFinalPriceSum);
        $entityManager->persist($product);

        $entityManager->flush();

        if (!$reservation) {
            throw $this->createNotFoundException(
                'No reservation found for id '.$id
            );
        }

        return $this->redirectToRoute('index');
    }
}
