<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $base_price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $final_price_sum;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="product")
     */
    private $reservations;

    public function __construct()
    {
        return $this->getName();
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBasePrice(): ?int
    {
        return $this->base_price;
    }

    public function setBasePrice(int $base_price): self
    {
        $this->base_price = $base_price;

        return $this;
    }

    public function getFinalPricesSum(): ?int
    {
        return $this->final_price_sum;
    }

    public function setFinalPricesSum(int $final_price_sum): self
    {
        $this->final_price_sum = $final_price_sum;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setProduct($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getProduct() === $this) {
                $reservation->setProduct(null);
            }
        }

        return $this;
    }

    public function calculateFinalPricesSum()
    {
        return $this->getFinalPricesSum() + $this->getBasePrice();
    }

    public function getUpdatedFinalPricesSum($entityManager, $productBasePrice)
    {
        $reservations = $this->getReservations();
        $newPrice = 0;

        if (!empty($reservations)) {
            foreach($reservations as $reservation) {
                $newPrice += $productBasePrice;
                $reservation->setBasePrice($productBasePrice);
                $reservation->setFinalPrice($productBasePrice);

                $entityManager->persist($reservation);
            }
        }

        return $newPrice;
    }
}
